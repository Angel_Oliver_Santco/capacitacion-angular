miapp = angular.module("miapp", ['ngResource'])


miapp.controller('sumaCtrl', function($scope){
	$scope.sumar = function(numero_1, numero_2){
		if(!numero_1) return;
		if(!numero_2) return;
		return numero_1 + numero_2;
	}
})

miapp.controller('directivaCtrl', function($scope){
	$scope.persona = {id:1, nombre: 'Angel', apellido: 'Oliver'};

})

miapp.controller('servicioCtrl', function($scope, PersonaService){
	$scope.personas = [];

	$scope.getPersonas = function() {
		PersonaService.get(
			function(resp){
				$scope.personas = resp;
			},
			function(error){
				alert(error);
			})
	}

	$scope.getPersonas();
})

miapp.directive('personaInfo', function() {
  return {
    templateUrl: 'views/persona-info.html',
    scope:{
    	persona: "=appPersona"
    },
    link: function(scope, elem, attr){

    }
  };
});

miapp.service('PersonaService', function($resource){
	return $resource('/api/persona.json', {}, {
		get: {method:'GET', isArray:true}
	});
})